#ifndef DMT143_H
#define DMT143_H

#include "AnalogSensor.h"
#include "DewPointSensor.h"
#include <string>

class DMT143 : public DewPointSensor, public AnalogSensor
{
public:
    DMT143(const pugi::xml_node configuration);
	~DMT143(void);

	float readDewPoint(void);

private:
	const float resistorValue_;
	const float dewPointRange_;// 30 - (-70) C
	const float currentRange_ ;// 20 - 4 mA
	const float minimumCurrent_ ;// 4 mA
	const float minimumDewPoint_ ;//-70C
};
#endif
