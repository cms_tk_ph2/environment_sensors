#ifndef DewPointSensor_H
#define DewPointSensor_H

#include <string>
#include <math.h>

#include "Sensor.h"
#include <tgmath.h>

class DewPointSensor : virtual public Sensor
{
public:
	DewPointSensor(const pugi::xml_node configuration)
: Sensor(configuration)
{;}
	~DewPointSensor(void){;}

	virtual float readDewPoint(void) = 0;

	float calculateTemperature(float humidity)
	{
	    float dewPoint = readDewPoint();
		return 243.04 * ( ( (17.625*dewPoint)/(243.04+dewPoint) ) - log(humidity/100) ) / ( 17.625 + log(humidity/100) - ( (17.625*dewPoint)/(243.04+dewPoint) ) );
	}

	float calculateHumidity(float temperature)
	{
	    float dewPoint = readDewPoint();
	    return  100 * ( exp( (17.625*dewPoint)/(243.04+dewPoint) ) / exp( (17.625*temperature)/(243.04+temperature) ) );
	}

};

#endif
