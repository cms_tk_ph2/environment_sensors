#include "SensorHandler.h"
#include "Sensor.h"
#include "PT100RTD.h"
#include "DMT143.h"
#include "DS18B20.h"
#include "HM2500.h"


#include <iostream>
#include <sstream>
#include <stdexcept>

/*!
************************************************
* SensorHandler constructor.
************************************************
*/
SensorHandler::SensorHandler() {}

/*!
************************************************
* SensorHandler distructor.
************************************************
*/
SensorHandler::~SensorHandler()
{
    for(auto it: fSensorMap) delete it.second;
    fSensorMap.clear();
}

/*!
************************************************
 * Load xml file.
 \param docPath Path to the xml input file.
 \param doc Pugi document to be uploaded.
 \return Pugi parser result.
************************************************
*/
pugi::xml_parse_result SensorHandler::loadXml(const std::string& docPath, pugi::xml_document& doc)
{
    pugi::xml_parse_result result = doc.load_file(docPath.c_str());
    return result;
}

/*!
****************************** ******************
 * Load xml file error handler.
 \param docPath Input xml file name.
 \param result Pugi parser result.
 \return False if no error occurred.
************************************************
*/
bool SensorHandler::loadXmlErrorHandler(const std::string& docPath, const pugi::xml_parse_result& result)
{
    std::cout << "Xml parse results for " << docPath << std::endl << "Parse error: " << result.description() << ", character pos = " << result.offset << std::endl;
    return false;
}

/*!
************************************************
 * Loads configuration from file testFileName
 * of the type described by testType and
 * executes test accordingly. Verbosity
 * is off by defualt but could be switched on.
 \param testFileName Configuration file.
 \param testType String containing the test
 type.
 \param verbose Sets verbosity on/off.
 \return False if something went wrong, true
 otherwise.
************************************************
*/
/*!
************************************************
 * Loads configuration from file testFileName
 * of the type described by testType and
 * executes test accordingly. Verbosity
 * is off by defualt but could be switched on.
 \param testFileName Configuration file.
 \param testType String containing the test
 type.
 \param verbose Sets verbosity on/off.
 \return False if something went wrong, true
 otherwise.
************************************************
*/
// bool SensorHandler::ExecuteTest(const std::string& testFileName, const
// std::string &testType, bool verbose)
// {
//   pugi::xml_document docConfigTest;
//   pugi::xml_parse_result result =
//   docConfigTest.load_file(testFileName.c_str()); if (!result)
//     return SensorHandler::LoadXmlErrorHandler(testFileName, result);
//   if (result && verbose)
//     std::cout << "Xml config loaded: " << result.description() << std::endl;
//   pugi::xml_node testNode = docConfigTest.child(testType.c_str());
//   if (!(testType.compare("iv_curve")))
//   {
//     itIVCurve testIV(testNode, verbose);
//     return testIV.Run();
//   }
//   else
//   {
//     std::cout
//       << "Test "
//       << testType
//       << " not definied or implemented!"
//       << std::endl
//       << "Aborting..."
//       << std::endl;
//     return false;
//   }
//   return true;
// }

/*!
************************************************
 * Loads pugi_xml document and from docPath and
 * finds every power supply's settings.
 * Multimeters and undefined models are not considered.
 * Verbosity boolean is set ON by defualt but
 * may be switched on.
 \param docPath Path to configuration file.
 \param docSettings Pugi_xml document.
 \param verbose Sets verbosity on/off.
 \return Settings of all declared power supplies;
 if it's empty then none has been found.
************************************************
*/
void SensorHandler::readSettings(const std::string& docPath, pugi::xml_document& docSettings, bool verbose)
{
    pugi::xml_parse_result result = SensorHandler::loadXml(docPath, docSettings);
    if(verbose)
    {
        if(result) { std::cout << "Xml config file loaded: " << result.description() << std::endl; }
        else
        {
            SensorHandler::loadXmlErrorHandler(docPath, result);
        }
    }
    fDocumentRoot = docSettings.child("Sensors");

    for(pugi::xml_node sensor = fDocumentRoot.child("Sensor"); sensor; sensor = sensor.next_sibling("Sensor"))
    {
        std::string inUse = sensor.attribute("InUse").value();
        if(inUse.compare("YES") != 0 && inUse.compare("Yes") != 0 && inUse.compare("yes") != 0) continue;

        std::string id    = sensor.attribute("ID").value();
        std::string model = sensor.attribute("Model").value();
        if(model.compare("PT100RTD") == 0) { fSensorMap.emplace(id, new PT100RTD(sensor)); }
        else if(model.compare("DMT143") == 0)
        {
            fSensorMap.emplace(id, new DMT143(sensor));
        }
        else if(model.compare("DS18B20") == 0)
        {
            fSensorMap.emplace(id, new DS18B20(sensor));
        }
        else if(model.compare("HM2500") == 0)
        {
            fSensorMap.emplace(id, new HM2500(sensor));
        }
        else
        {
            std::stringstream error;
            error << "The Model: " << model
                  << " is not an available sensor and won't be initialized, "
                     "please check the xml configuration file.";
            throw std::runtime_error(error.str());
        }
    }
}

Sensor* SensorHandler::getSensor(const std::string& id)
{
    if(fSensorMap.find(id) == fSensorMap.end()) { throw std::out_of_range("No power supply with id " + id + " has been configured!"); }
    return fSensorMap.find(id)->second;
}
