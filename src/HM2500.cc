#include "HM2500.h"

#include <iostream>
#include <string>
#include <fstream>
#include <iomanip>

const float R1 = 100;
const float R2 = 120;
const float R0 = 100;

//===========================================================================
HM2500::HM2500(const pugi::xml_node configuration)
: Sensor        (configuration)
, HumiditySensor(configuration)
, AnalogSensor  (configuration)
{
	std::string temperatureId = fId.substr(0, fId.find(','));
	std::string humidityId    = fId.substr(fId.find(',')+1);
	temperaturePath_          = "/sys/bus/iio/devices/iio:device0/" + temperatureId;
	humidityPath_             = "/sys/bus/iio/devices/iio:device0/" + humidityId;
}

//===========================================================================
float HM2500::readHumidity(void)
{
	float Vout = AnalogSensor::readValue(sensorPath_) * 1000;
	return -1.92*(pow(10,-9))*pow(Vout,3) + 1.44*(pow(10,-5))*pow(Vout,2) + 3.4*(pow(10,-3))*Vout -12.4;
}

