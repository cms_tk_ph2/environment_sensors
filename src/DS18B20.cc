#include "DS18B20.h"

#include <iostream>
#include <string>
#include <fstream>
#include <iomanip>

//===========================================================================
DS18B20::DS18B20(const pugi::xml_node configuration)
: Sensor           (configuration)
, TemperatureSensor(configuration)
, sensorPath_      (std::string("/sys/bus/w1/devices/") + configuration.attribute("ID").value() + "/w1_slave")
{
	std::ifstream file;
	std::string line;
	const std::string capemgrFileName = "/sys/devices/bone_capemgr.9/slots";

	file.open(capemgrFileName.c_str());
	if(!file.is_open())
	{
		std::cout << "IMPOSSIBLE: Could not open device configuration file: " << capemgrFileName << ". Crashing now..." << std::endl;
		exit(0);
	}
    bool found = false;
	while (getline(file,line))
    {
		if(line.find("Manuf,w1") != std::string::npos || line.find("Manuf,W1") != std::string::npos)
		{
			found = true;
			break;
		}
    }

	if(!found)
		system(("echo BB-W1 > " + capemgrFileName).c_str());
	file.close();
}

//===========================================================================
DS18B20::~DS18B20(void)
{;}

//===========================================================================
float DS18B20::readTemperature(void)
{
	float temperature = 0;
	std::ifstream file;
	char buf[256];

	file.open(sensorPath_.c_str());

	if(file.is_open())
	{
		file.read(buf, 256);
		std::string buffer(buf);
		temperature = strtof(buffer.substr(buffer.find("t=")+2).c_str(), NULL)/1000.;
		//std::cout << __PRETTY_FUNCTION__ << "Device: " << fId << " - " << "Temperature: " << std::fixed << std::setprecision(3) << temperature << "C " << std::endl;
		file.close();
	}
	else
		std::cout << "Could not open sensor: " << fId << "." << std::endl;

	return temperature;
} 
