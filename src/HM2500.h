#ifndef HM2500_H
#define HM2500_H

#include "AnalogSensor.h"
#include "HumiditySensor.h"
#include <string>

class HM2500 : public HumiditySensor, public AnalogSensor
{
public:
    HM2500(const pugi::xml_node configuration);
	~HM2500(void) {;}

	float readHumidity(void);

private:
	std::string temperaturePath_;
	std::string humidityPath_;
//	float dataValue_; //in Celsius
};
#endif
