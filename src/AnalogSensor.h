#ifndef AnalogSensor_H
#define AnalogSensor_H

#include "Sensor.h"

class AnalogSensor : virtual public Sensor
{
public:
 AnalogSensor (const pugi::xml_node configuration);
 ~AnalogSensor(void);
  
 float readValue(std::string path);
protected:
 std::string sensorPath_;
};

#endif
