#ifndef Sensor_H
#define Sensor_H

#include "pugixml.hpp"
#include <string>

class Sensor
{
public:
	Sensor(const pugi::xml_node configuration)
: fConfiguration(configuration)
, fId           (fConfiguration.attribute("ID").value())
, fModel        (fConfiguration.attribute("Model").value())
{;}
	virtual ~Sensor(void){;}

	void        setId(std::string id      ){fId    = id;}
	void        setModel(std::string model){fModel = model;}
	std::string getId(void                ){return fId;}
	std::string getModel(void             ){return fModel;}

protected:
    const pugi::xml_node fConfiguration;
	std::string fId;
	std::string fModel;
};

#endif
