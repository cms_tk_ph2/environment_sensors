/*!
 * \authors Mattia Lizzo <mattia.lizzo@cern.ch>, INFN-Firenze
 * \authors Francesco Fiori <francesco.fiori@cern.ch>, INFN-Firenze
 * \authors Antonio Cassese <antonio.cassese@cern.ch>, INFN-Firenze
 * \authors Lorenzo Uplegger <uplegger@fnal.gov>, Fermilab
 * \date Sep 2 2019
 */

#ifndef SensorHandler_H
#define SensorHandler_H
//#include "Multimeter.h"
#include "pugixml.hpp"
#include <string>
#include <unordered_map>
#include <vector>

/*!
************************************************
 \class SensorHandler.
 \brief SensorHandler class for power supplies management.
************************************************
*/
class Sensor;

class SensorHandler
{
  public:
    SensorHandler();
    virtual ~SensorHandler();
    void    readSettings(const std::string& docPath, pugi::xml_document& doc, bool verbose = true);
    Sensor* getSensor(const std::string& id);

  private:
    // Variables
    pugi::xml_node                           fDocumentRoot;
    std::unordered_map<std::string, Sensor*> fSensorMap;

    // Test and xml handling methods
    static pugi::xml_parse_result loadXml(const std::string& docPath, pugi::xml_document& doc);
    static bool                   loadXmlErrorHandler(const std::string& docPath, const pugi::xml_parse_result& result);
};

#endif
