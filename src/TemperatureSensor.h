#ifndef TemperatureSensor_H
#define TemperatureSensor_H

#include <string>
#include "Sensor.h"
#include <tgmath.h>

class TemperatureSensor : virtual public Sensor
{
public:
	TemperatureSensor(const pugi::xml_node configuration)
: Sensor(configuration)
{;}

	~TemperatureSensor(void){;}

	virtual float readTemperature(void) = 0;

	float calculateHumidity(float dewPoint)
	{
		float temperature = readTemperature();
		return  100 * ( exp( (17.625*dewPoint)/(243.04+dewPoint) ) / exp( (17.625*temperature)/(243.04+temperature) ) );
	}

	float calculateDewPoint(float humidity)
	{
		float temperature = readTemperature();
		return 243.04 * ( log(humidity/100) + ( (17.625*temperature)/(243.04+temperature) ) ) / ( 17.625 - log(humidity/100) - ( (17.625*temperature)/(243.04+temperature) ) );
	}
};

#endif
