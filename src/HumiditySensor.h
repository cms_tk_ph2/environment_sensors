#ifndef HumiditySensor_H
#define HumiditySensor_H

#include "Sensor.h"

#include <string>
#include <math.h>
#include <tgmath.h>

class HumiditySensor : virtual public Sensor
{
public:
	HumiditySensor(const pugi::xml_node configuration)
: Sensor(configuration)
{;}
	~HumiditySensor(void){;}

	virtual float readHumidity(void) = 0;

	float calculateTemperature(float dewPoint)
	{
		float humidity = readHumidity();
		return 243.04 * ( ( (17.625*dewPoint)/(243.04+dewPoint) ) - log(humidity/100) ) / ( 17.625 + log(humidity/100) - ( (17.625*dewPoint)/(243.04+dewPoint) ) );
	}

	float calculateDewPoint(float temperature)
	{
		float humidity = readHumidity();
		return 243.04 * ( log(humidity/100) + ( (17.625*temperature)/(243.04+temperature) ) ) / ( 17.625 - log(humidity/100) - ( (17.625*temperature)/(243.04+temperature) ) );
	}

};
#endif
