#ifndef _ots_DS18B20_h_
#define _ots_DS18B20_h_

#include "TemperatureSensor.h"
#include <string>

class DS18B20 : public TemperatureSensor
{
public:
    DS18B20(const pugi::xml_node configuration);
	~DS18B20(void);

	float readTemperature(void);

private:
	const std::string sensorPath_;
};
#endif
