#ifndef PT100RTD_H
#define PT100RTD_H

#include "AnalogSensor.h"
#include "TemperatureSensor.h"
#include <string>

class PT100RTD : public TemperatureSensor, public AnalogSensor
{
public:
    PT100RTD(const pugi::xml_node configuration);
	~PT100RTD(void);

	float readTemperature(void);

private:
};
#endif
